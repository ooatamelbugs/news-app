FROM node:alpine As development

# set the workdir
WORKDIR  /usr/src/app

# copy file package and yarn to app workdir
# COPY package.json yarn.lock /app/
COPY package*.json ./

COPY prisma ./prisma/

# run yarn 
RUN yarn add glob rimraf

RUN yarn --only=development

# copy all file to app
COPY . .

# build app
RUN yarn build

# run start app
# CMD [ "yarn", "start" ]

FROM node:alpine As production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./
COPY prisma ./prisma/

# run yarn 
RUN yarn add glob rimraf

RUN yarn --only=production

COPY . .

COPY --from=development /usr/src/app/dist ./dist

CMD [ "node", "dist/main" ]
