import { IsNotEmpty } from 'class-validator';

export class NewsImageDTO {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  url: string;

  @IsNotEmpty()
  newsid: string;
}

export class GetNewsImageDTO {
  title?: string;
  id?: string;
  url?: string;
  newsid?: string;
}
