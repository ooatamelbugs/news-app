import { PrismaService } from './../prisma/prisma.service';
import { Injectable } from '@nestjs/common';
import { ImageNews } from '@prisma/client';
import { NewsImageDTO, GetNewsImageDTO } from './dto';
import { ImageNewsInterface } from './interface';

@Injectable()
export class NewsImageRepository implements ImageNewsInterface {
  constructor(private prisme: PrismaService) {}

  async getBy(getby: Partial<GetNewsImageDTO>): Promise<ImageNews> {
    return this.prisme.imageNews.findFirst({ where: getby });
  }

  async createImage(news: NewsImageDTO): Promise<ImageNews> {
    return this.prisme.imageNews.create({ data: news });
  }

  async updateImage(
    imageId: string,
    news: Partial<NewsImageDTO>,
  ): Promise<ImageNews> {
    return this.prisme.imageNews.update({
      where: { id: imageId },
      data: news,
    });
  }

  async deleteImage(imageId: string): Promise<boolean> {
    this.prisme.imageNews.delete({ where: { id: imageId } });
    return true;
  }
}
