import { Module } from '@nestjs/common';
import { NewsimageService } from './newsimage.service';
import { NewsimageController } from './newsimage.controller';
import { NewsImageRepository } from './newsimage.repository';

@Module({
  providers: [NewsimageService, NewsImageRepository],
  controllers: [NewsimageController],
  exports: [NewsimageService],
})
export class NewsimageModule {}
