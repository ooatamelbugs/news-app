import { ImageNews } from '@prisma/client';
import { NewsImageDTO, GetNewsImageDTO } from '../dto';

export interface ImageNewsInterface {
  getBy(getby: Partial<GetNewsImageDTO>): Promise<ImageNews>;
  createImage(news: NewsImageDTO): Promise<ImageNews>;
  updateImage(imageId: string, news: Partial<NewsImageDTO>): Promise<ImageNews>;
  deleteImage(imageId: string): Promise<boolean>;
}
