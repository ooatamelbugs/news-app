import { Injectable, ForbiddenException } from '@nestjs/common';
import { NewsImageRepository } from './newsimage.repository';
import { NewsImageDTO } from './dto';
@Injectable()
export class NewsimageService {
  constructor(private newsImageRepository: NewsImageRepository) {}

  async updateimage(imageId: string, imageData: Partial<NewsImageDTO>) {
    try {
      const data = await this.newsImageRepository.getBy({ id: imageId });
      if (!data) {
        return new ForbiddenException('this record is not exist');
      }
      return await this.newsImageRepository.updateImage(data.id, imageData);
    } catch (error) {
      return new ForbiddenException('error in update data');
    }
  }

  async deleteimage(imageId: string) {
    try {
      const data = await this.newsImageRepository.getBy({ id: imageId });
      if (!data) {
        return new ForbiddenException('this record is not exist');
      }
      return await this.newsImageRepository.deleteImage(data.id);
    } catch (error) {
      return new ForbiddenException('error in update data');
    }
  }
}
