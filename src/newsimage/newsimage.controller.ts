import { JwtGuard } from './../auth/guard/auth.guard';
import { NewsimageService } from './newsimage.service';
import {
  Controller,
  Param,
  Patch,
  Delete,
  Body,
  UseGuards,
} from '@nestjs/common';
import { NewsImageDTO } from './dto';
import { ApiTags, ApiParam, ApiResponse } from '@nestjs/swagger';

@UseGuards(JwtGuard)
@ApiTags('newsimage')
@Controller('newsimage')
export class NewsimageController {
  constructor(private newsimageService: NewsimageService) {}

  @Patch('/:id')
  async updateNewsimage(
    @Param('id') id: string,
    @Body() data: Partial<NewsImageDTO>,
  ) {
    return await this.newsimageService.updateimage(id, data);
  }

  @Delete('/:id')
  async deleteNewsimage(@Param('id') id: string) {
    return await this.newsimageService.deleteimage(id);
  }
}
