import { SreachCategoryDTO } from './dto';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { categoryDTO } from './dto';
import { CategoryRepository } from './category.repository';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { Category } from '@prisma/client';

@Injectable()
export class CategoryService {
  constructor(private readonly categoryRepo: CategoryRepository) {}

  async createCategory(data: categoryDTO): Promise<Category | Error> {
    try {
      const newCategory = await this.categoryRepo.createCategory(data);
      return newCategory;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new ForbiddenException('exist taken');
        }
      }
    }
  }

  async getCategory(): Promise<Array<Category | null>> {
    return await this.categoryRepo.getAllCategory();
  }

  async getOneCategory(
    researchData: SreachCategoryDTO,
  ): Promise<Category | null> {
    return this.categoryRepo.getCategoryByAny(researchData);
  }

  async updateCategory(
    categoryId: string,
    dataUpdate: Partial<categoryDTO>,
  ): Promise<Category | Error> {
    try {
      const existData = await this.categoryRepo.getCategoryByAny({
        id: categoryId,
      });
      if (!existData) {
        return new ForbiddenException('this record is not exist');
      }
      const update = await this.categoryRepo.updateCategoryById(
        existData.id,
        dataUpdate,
      );
      return update;
    } catch (error) {
      return new ForbiddenException('error in update data');
    }
  }

  async deleteCategory(categoryId: string): Promise<boolean | Error> {
    try {
      const existData = await this.categoryRepo.getCategoryByAny({
        id: categoryId,
      });
      if (existData) {
        return new ForbiddenException('this record is not exist');
      }
      const deleteCategory = await this.categoryRepo.deleteCategoryById(
        existData.id,
      );
      return deleteCategory;
    } catch (error) {
      return new ForbiddenException('error in update data');
    }
  }
}
