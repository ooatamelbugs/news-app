import { categoryDTO, SreachCategoryDTO } from './../dto';
import { Category } from '@prisma/client';

export interface CategoryInterface {
  createCategory(categoryData: categoryDTO): Promise<Category>;
  getCategoryByAny(category: SreachCategoryDTO): Promise<Category>;
  getAllCategory(): Promise<Array<Category | null>>;
  deleteCategoryById(categoryId: string): Promise<boolean>;
  updateCategoryById(
    categoryId: string,
    data: Partial<categoryDTO>,
  ): Promise<Category>;
}
