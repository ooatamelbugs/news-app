import { IsNotEmpty } from 'class-validator';

export class categoryDTO {
  @IsNotEmpty()
  title: string;
  image?: string;
}

export class SreachCategoryDTO {
  title?: string;
  id?: string;
}
