import { CategoryInterface } from './interface/category';
import { PrismaService } from './../prisma/prisma.service';
import { Category } from '@prisma/client';
import { categoryDTO, SreachCategoryDTO } from './dto';

export class CategoryRepository implements CategoryInterface {
  constructor(private prisma: PrismaService) {}

  async createCategory(categoryData: categoryDTO): Promise<Category> {
    return this.prisma.category.create({
      data: categoryData,
    });
  }

  async getCategoryByAny(category: SreachCategoryDTO): Promise<Category> {
    return this.prisma.category.findFirst({
      where: category,
    });
  }

  async getAllCategory(): Promise<Array<Category | null>> {
    return this.prisma.category.findMany();
  }

  async deleteCategoryById(categoryId: string): Promise<boolean> {
    const deleteCategory = await this.prisma.category.delete({
      where: {
        id: categoryId,
      },
    });
    return !deleteCategory;
  }

  async updateCategoryById(
    categoryId: string,
    data: Partial<categoryDTO>,
  ): Promise<Category> {
    return this.prisma.category.update({
      where: {
        id: categoryId,
      },
      data,
    });
  }
}
