import { CategoryService } from './category.service';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CategoryController } from './category.controller';
import { CategoryRepository } from './category.repository';

@Module({
  imports: [ConfigService],
  controllers: [CategoryController],
  providers: [CategoryService, CategoryRepository],
})
export class CategoryModule {}
