import { JwtGuard } from './../auth/guard/auth.guard';
import { categoryDTO } from './dto';
import { CategoryService } from './category.service';
import {
  Controller,
  Get,
  Post,
  Patch,
  Delete,
  Body,
  UseGuards,
  Param,
} from '@nestjs/common';
import { ApiTags, ApiParam, ApiResponse } from '@nestjs/swagger';

@ApiTags('category')
@Controller('category')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: '',
    type: [categoryDTO],
  })
  async getAll() {
    return await this.categoryService.getCategory();
  }

  @Get('/:id')
  @ApiParam({
    name: 'id',
    required: true,
    description: '',
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: '',
    type: categoryDTO,
  })
  @ApiResponse({
    status: 404,
    description: '',
    type: categoryDTO,
  })
  async geOne(@Param('id') id: string) {
    return await this.categoryService.getOneCategory({ id });
  }

  @UseGuards(JwtGuard)
  @Post()
  @ApiResponse({
    status: 201,
    description: '',
    type: categoryDTO,
  })
  async createCategory(@Body() data: categoryDTO) {
    return await this.categoryService.createCategory(data);
  }

  @UseGuards(JwtGuard)
  @Patch('/:id')
  @ApiParam({
    name: 'id',
    required: true,
    description: '',
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: '',
    type: categoryDTO,
  })
  @ApiResponse({
    status: 404,
    description: '',
  })
  async updateCategory(
    @Param('id') id: string,
    @Body() data: Partial<categoryDTO>,
  ) {
    return await this.categoryService.updateCategory(id, data);
  }

  @UseGuards(JwtGuard)
  @Delete('/:id')
  @ApiParam({
    name: 'id',
    required: true,
    description: '',
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: '',
    type: Boolean,
  })
  @ApiResponse({
    status: 404,
    description: '',
  })
  async deleteCategory(@Param('id') id: string) {
    return await this.categoryService.deleteCategory(id);
  }
}
