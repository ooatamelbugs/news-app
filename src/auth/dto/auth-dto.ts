import { IsEmail, IsNotEmpty } from 'class-validator';

export class loginDTO {
  @IsEmail()
  @IsNotEmpty({
    message: 'username not allow to be impty',
  })
  username: string;
  @IsNotEmpty({
    message: 'password not allow to be impty',
  })
  password: string;
}
