import { AdminDTO } from './../admin/dto/admin-dto';
import { AuthService } from './auth.service';
import { loginDTO } from './dto';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags, ApiParam, ApiResponse } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signin')
  @ApiResponse({
    status: 200,
    description: 'api for  all login admin',
    type: AdminDTO,
  })
  async login(@Body() logindata: loginDTO) {
    return this.authService.login(logindata);
  }

  @Post('/signup')
  @ApiResponse({
    status: 200,
    description: 'api for  all rigester admin',
    type: AdminDTO,
  })
  async rigester(@Body() rigBody: AdminDTO) {
    return this.authService.rigester(rigBody);
  }
}
