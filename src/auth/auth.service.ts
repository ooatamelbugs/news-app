import { PrismaService } from './../prisma/prisma.service';
import { Admin } from '@prisma/client';
import { AdminDTO } from './../admin/dto/admin-dto';
import { loginDTO } from './dto';
import { ForbiddenException, Injectable } from '@nestjs/common';
import * as argon from 'argon2';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtservers: JwtService,
    private configservice: ConfigService,
  ) {}

  async login(data: loginDTO) {
    const dataAdmin = await this.prisma.admin.findFirst({
      where: { username: data.username },
    });
    if (!data) throw new ForbiddenException('invalid admin');
    const verifypass = await argon.verify(dataAdmin.password, data.password);
    if (!verifypass) throw new ForbiddenException('invalid admin');
    delete dataAdmin.password;
    const token = this.generateToken(dataAdmin.id, dataAdmin.username);
    return { data: { user: dataAdmin, token } };
  }

  async rigester(data: AdminDTO): Promise<Admin> {
    return this.prisma.admin.create({
      data: {
        username: data.username,
        password: data.password,
        fullname: data.fullname,
        image: data.fullname,
        isAdmin: true,
      },
    });
  }

  generateToken(userId: string, email: string): Promise<string> {
    const payload = { sub: userId, email: email };
    return this.jwtservers.signAsync(payload, {
      expiresIn: '30m',
      secret: this.configservice.get('JWT_SECRET'),
    });
  }
}
