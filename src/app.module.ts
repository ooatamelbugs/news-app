import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { AuthModule } from './auth/auth.module';
import { AdminService } from './admin/admin.service';
import { AdminModule } from './admin/admin.module';
import { NewsModule } from './news/news.module';
import { CategoryService } from './category/category.service';
import { CategoryModule } from './category/category.module';
import { NewsimageModule } from './newsimage/newsimage.module';
import { UploadController } from './upload/upload.controller';
import { UploadModule } from './upload/upload.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PrismaModule,
    AuthModule,
    AdminModule,
    NewsModule,
    CategoryModule,
    NewsimageModule,
    UploadModule,
  ],
})
export class AppModule {}
