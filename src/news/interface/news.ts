import { News } from '@prisma/client';
import { NewsDTO, SreachNewsDTO } from '../dto';
import { NewsImageDTO } from 'src/newsimage/dto';

export interface NewsInterface {
  createNews(news: NewsDTO, images: Array<NewsImageDTO>): Promise<News>;
  getNews(
    skip: number,
    limit?: number,
    order?: [any],
    sreach?: Partial<NewsDTO>,
  ): Promise<Array<News>>;
  getOneNews(sreach: Partial<SreachNewsDTO>): Promise<News>;
  updateNews(newsId: string, data: Partial<NewsDTO>): Promise<News>;
  deleteNews(newsId: string): Promise<boolean>;
}
