import {
  Controller,
  Body,
  Query,
  Param,
  Post,
  Patch,
  Get,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsDTO, BodyNewsDTO, SreachNewsDTO } from './dto';
import { JwtGuard } from './../auth/guard';
import { AdminDecorator } from './../auth/decorator';
import { ApiTags, ApiParam, ApiResponse } from '@nestjs/swagger';

@ApiTags('news')
@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @UseGuards(JwtGuard)
  @Post()
  async create(@Body() data: BodyNewsDTO, @AdminDecorator('sub') sub: string) {
    data.news.adminId = sub;
    return await this.newsService.createNews(data.news, data.images);
  }

  @UseGuards(JwtGuard)
  @Patch()
  async update(
    @Param('id') id: string,
    @Body() data: Partial<NewsDTO>,
    @AdminDecorator('sub') sub: string,
  ) {
    data.adminId = sub;
    return await this.newsService.updateNews(id, data);
  }

  @Get('/:id')
  async getOneNews(@Param('id') id: string) {
    return await this.newsService.getOneNews({ id });
  }

  @UseGuards(JwtGuard)
  @Delete('/:id')
  async deleteNews(@Param('id') id: string) {
    return await this.newsService.deleteNews(id);
  }

  @Get('')
  async getAll(
    @Query('skip') skip: number,
    @Query('limit') limit: number,
    @Query('orderBy') orderBy?: string,
    @Query('sortType') sortType?: string,
    @Body() data?: SreachNewsDTO,
  ) {
    return this.newsService.getAllNews(skip, limit, data, orderBy, sortType);
  }
}
