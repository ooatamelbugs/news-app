import { NewsRepository } from './news.repository';
import { Injectable, ForbiddenException } from '@nestjs/common';
import { NewsDTO, SreachNewsDTO } from './dto';
import { News } from '@prisma/client';
import { NewsImageDTO } from 'src/newsimage/dto';

@Injectable()
export class NewsService {
  constructor(private newsRepository: NewsRepository) {}

  async createNews(
    news: NewsDTO,
    images: Array<NewsImageDTO | null>,
  ): Promise<News | Error> {
    try {
      return await this.newsRepository.createNews(news, images);
    } catch (error) {
      return new ForbiddenException('error in create data');
    }
  }

  async updateNews(
    newsID: string,
    news: Partial<NewsDTO>,
  ): Promise<News | Error> {
    try {
      const newsRecord = await this.newsRepository.getOneNews({ id: newsID });
      if (!newsRecord) {
        return new ForbiddenException('this record is not exist');
      }
      return await this.newsRepository.updateNews(newsID, news);
    } catch (error) {
      return new ForbiddenException('error in update data');
    }
  }

  async getAllNews(
    skip: number,
    limit?: number,
    sreach?: SreachNewsDTO,
    sortBy?: string,
    sortType?: string,
  ): Promise<Array<News> | null> {
    const sort = [];
    if (sortBy) {
      const object: SreachNewsDTO = {};
      object[sortBy] = sortType;
      sort.push(object);
    }
    return this.newsRepository.getNews(skip, limit, sort, sreach);
  }

  async getOneNews(sreach: Partial<SreachNewsDTO>): Promise<News | Error> {
    try {
      const newsRecord = await this.newsRepository.getOneNews(sreach);
      if (!newsRecord) {
        return new ForbiddenException('this record is not exist');
      }
      return newsRecord;
    } catch (error) {
      return new ForbiddenException('error in update data');
    }
  }

  async deleteNews(newsID: string): Promise<boolean | Error> {
    try {
      const newsRecord = await this.newsRepository.getOneNews({ id: newsID });
      if (!newsRecord) {
        return new ForbiddenException('this record is not exist');
      }
      return await this.newsRepository.deleteNews(newsRecord.id);
    } catch (error) {
      return new ForbiddenException('error in delete data');
    }
  }
}
