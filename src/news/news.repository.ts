import { NewsImageRepository } from './../newsimage/newsimage.repository';
import { NewsInterface } from './interface';
import { News } from '@prisma/client';
import { NewsDTO, SreachNewsDTO } from './dto';
import { PrismaService } from './../prisma/prisma.service';
import { NewsImageDTO } from 'src/newsimage/dto';

export class NewsRepository implements NewsInterface {
  constructor(
    private prisma: PrismaService,
    private newsImageRepository: NewsImageRepository,
  ) {}
  async createNews(
    newsdata: NewsDTO,
    images: Array<NewsImageDTO | null>,
  ): Promise<News> {
    const news = await this.prisma.news.create({ data: newsdata });
    for (const imagedata of images) {
      imagedata.newsid = news.id;
      const image = await this.newsImageRepository.createImage(imagedata);
    }
    return news;
  }

  async getNews(
    skip: number,
    limit?: number,
    order?: any[],
    sreach?: Partial<NewsDTO>,
  ): Promise<Array<News>> {
    return this.prisma.news.findMany({
      where: sreach || null,
      skip,
      take: limit || 20,
      orderBy: order || [],
    });
  }

  async getOneNews(sreach: Partial<SreachNewsDTO>): Promise<News> {
    return this.prisma.news.findFirst({ where: sreach });
  }
  async updateNews(newsId: string, dataNews: Partial<NewsDTO>): Promise<News> {
    return this.prisma.news.update({ where: { id: newsId }, data: dataNews });
  }

  async deleteNews(newsId: string): Promise<boolean> {
    this.prisma.news.delete({ where: { id: newsId } });
    return true;
  }
}
