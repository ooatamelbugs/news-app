import { IsArray, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { NewsImageDTO } from 'src/newsimage/dto';

export class NewsDTO {
  id?: string;
  @IsString()
  @IsNotEmpty()
  title: string;
  desc: string;
  status: boolean;
  @IsNumber()
  views: number;
  @IsNotEmpty()
  @IsString()
  categoryId: string;
  @IsNotEmpty()
  @IsString()
  adminId: string;
}

export class SreachNewsDTO {
  title?: string;
  desc?: string;
  categoryId?: string;
  adminId?: string;
  id?: string;
  status?: boolean;
  views?: number;
}

export class BodyNewsDTO {
  news: NewsDTO;
  @IsArray()
  images: Array<NewsImageDTO | null>;
}
