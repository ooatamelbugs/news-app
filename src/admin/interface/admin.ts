import { AdminDTO, SreachAdminDTO, UpdateAdminDTO } from './../dto/admin-dto';
import { Admin } from '@prisma/client';

export interface AdminInterface {
  createAdmin(admin: AdminDTO): Promise<Admin>;
  getAdmin(sreachAdmin: Partial<SreachAdminDTO>): Promise<Admin>;
  deleteAdmin(adminId: string): Promise<boolean>;
  updateAdmin(adminId: string, updateAdmin: UpdateAdminDTO): Promise<Admin>;
  getAllAdmin(
    skip: number,
    limit?: number,
    order?: [any],
    sreach?: SreachAdminDTO,
  ): Promise<Array<Admin>>;
}
