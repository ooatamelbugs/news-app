import { AdminDTO, SreachAdminDTO, UpdateAdminDTO } from './dto/admin-dto';
import { AdminRepository } from './admin.repository';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { Admin } from '@prisma/client';
import * as argon from 'argon2';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';

@Injectable()
export class AdminService {
  constructor(private adminRepo: AdminRepository) {}

  async create(admin: AdminDTO): Promise<Admin> {
    try {
      const hash = await argon.hash(admin.password);
      admin.password = hash;
      const newAdmin = await this.adminRepo.createAdmin(admin);
      return newAdmin;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new ForbiddenException('credentinal taken');
        }
      }
    }
  }

  async get(sreach: SreachAdminDTO) {
    return await this.adminRepo.getAdmin(sreach);
  }

  async updateAdmin(
    adminId: string,
    updateAdminData: UpdateAdminDTO,
  ): Promise<Admin> {
    if (updateAdminData.password) {
      const hash = await argon.hash(updateAdminData.password);
      updateAdminData.password = hash;
    }
    return await this.adminRepo.updateAdmin(adminId, updateAdminData);
  }

  async deleteAdmin(adminId: string) {
    return await this.adminRepo.deleteAdmin(adminId);
  }

  async getAll(
    skip: number,
    limit?: number,
    sreach?: SreachAdminDTO,
    sortBy?: string,
    sortType?: string,
  ) {
    const sort = [];
    if (sortBy) {
      const object: SreachAdminDTO = {};
      object[sortBy] = sortType;
      sort.push(object);
    }
    return await this.adminRepo.getAllAdmin(skip, limit, sort, sreach);
  }
}
