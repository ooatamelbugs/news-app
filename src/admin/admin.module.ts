import { AdminRepository } from './admin.repository';
import { AdminService } from './admin.service';
import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';

@Module({
  controllers: [AdminController],
  providers: [AdminRepository, AdminService],
  exports: [AdminService, AdminRepository],
})
export class AdminModule {}
