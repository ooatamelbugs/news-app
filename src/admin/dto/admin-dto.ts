import { IsEmail, IsNotEmpty } from 'class-validator';

export class AdminDTO {
  @IsEmail()
  @IsNotEmpty()
  username: string;

  @IsEmail()
  @IsNotEmpty()
  password: string;

  fullname?: string;
  image?: string;
  isAdmin?: boolean;
}

export class UpdateAdminDTO {
  username?: string;

  password?: string;

  fullname?: string;
  image?: string;
  isAdmin?: boolean;
}

export class SreachAdminDTO {
  username?: string;
  fullname?: string;
  id?: string;
}
