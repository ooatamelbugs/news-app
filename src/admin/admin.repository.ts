import { AdminDTO, SreachAdminDTO, UpdateAdminDTO } from './dto/admin-dto';
import { PrismaService } from './../prisma/prisma.service';
import { Admin } from '@prisma/client';
import { AdminInterface } from './interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AdminRepository implements AdminInterface {
  constructor(private prisma: PrismaService) {}
  async createAdmin(admin: AdminDTO): Promise<Admin> {
    return await this.prisma.admin.create({
      data: {
        username: admin.username,
        password: admin.password,
        fullname: admin.fullname,
        image: admin.fullname,
        isAdmin: true,
      },
    });
  }
  async getAdmin(sreachAdmin: Partial<SreachAdminDTO>): Promise<Admin> {
    return await this.prisma.admin.findFirst({
      where: sreachAdmin,
    });
  }

  async deleteAdmin(adminId: string): Promise<boolean> {
    await this.prisma.admin.delete({
      where: {
        id: adminId,
      },
    });
    return true;
  }
  async updateAdmin(
    adminId: string,
    updateAdmin: UpdateAdminDTO,
  ): Promise<Admin> {
    return await this.prisma.admin.update({
      where: {
        id: adminId,
      },
      data: updateAdmin,
    });
  }

  async getAllAdmin(
    skip: number,
    limit?: number,
    order?: any[],
    sreach?: SreachAdminDTO,
  ): Promise<Array<Admin>> {
    return await this.prisma.admin.findMany({
      where: sreach || null,
      skip,
      take: limit || 20,
      orderBy: order || [],
    });
  }
}
