import { UpdateAdminDTO, AdminDTO, SreachAdminDTO } from './dto';
import { AdminDecorator } from './../auth/decorator';
import { JwtGuard } from './../auth/guard';
import { AdminService } from './admin.service';
import {
  Body,
  Controller,
  Get,
  Put,
  NotFoundException,
  UseGuards,
  Query,
  Type,
} from '@nestjs/common';
import { ApiTags, ApiParam, ApiResponse } from '@nestjs/swagger';

@UseGuards(JwtGuard)
@ApiTags('admin')
@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: 'api for  all getUserByUsername admin',
    type: AdminDTO,
  })
  async getUserByUsername(@AdminDecorator('username') username: string) {
    return this.adminService.get({ username });
  }

  @Put()
  @ApiResponse({
    status: 200,
    description: 'api for  all updateUser admin',
    type: AdminDTO,
  })
  @ApiResponse({
    status: 404,
    description: 'admin not',
  })
  async updateUser(
    @Body() data: UpdateAdminDTO,
    @AdminDecorator('sub') sub: string,
  ) {
    const dataUser = await this.adminService.get({ username: data.username });
    if (!dataUser) {
      throw new NotFoundException('not admin');
    }
    const update = await this.adminService.updateAdmin(sub, data);
    delete update.password;
    return update;
  }

  @Get('/all')
  @ApiResponse({
    status: 200,
    description: 'api for get all admin',
    type: [AdminDTO],
  })
  async getAll(
    @Query('skip') skip: number,
    @Query('limit') limit: number,
    @Query('orderBy') orderBy?: string,
    @Query('sortType') sortType?: string,
    @Body() data?: SreachAdminDTO,
  ) {
    return this.adminService.getAll(skip, limit, data, orderBy, sortType);
  }
}
