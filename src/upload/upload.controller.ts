import {
  Controller,
  Post,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import * as path from 'path';

const fileFilter = (req, file, callback) => {
  const extn = path.extname(file.orginalname);
  let error, status;
  switch (extn) {
    case '.png':
    case '.jpeg':
    case '.jpg':
      error = null;
      status = true;
      break;
    default:
      req.fileValidationError = 'INVALID TYPE';
      error = new Error('invalid file type');
      status = false;
      break;
  }
  return callback(error, status);
};

@Controller('image')
export class UploadController {
  @Post('upload')
  @UseInterceptors(
    FilesInterceptor('images[]', 10, {
      dest: './uploads',
      fileFilter,
    }),
  )
  uploadImages(@UploadedFiles() files: Array<Express.Multer.File>) {
    console.log(files);
    return files;
  }
}
